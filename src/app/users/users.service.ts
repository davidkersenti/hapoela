import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2'; 


@Injectable()
export class UsersService {
 // private _url = "http://jsonplaceholder.typicode.com/users";

 usersObservable;
  
  constructor(private af:AngularFire) { }

  addUser(user){
    this.usersObservable.push(user);
  }

  getUsers(){
    this.usersObservable = this.af.database.list('/users');
    return this.usersObservable;
	}
}