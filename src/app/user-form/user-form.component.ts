import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {User} from '../user/user'

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Output() userAddedEvent = new EventEmitter<User>();
  user:User = {
    name: '',
    email: ''
  };
  //usr:User = {name:'',email:''};  
  constructor() { }

  onSubmit(form:NgForm){
    console.log(form);
    this.userAddedEvent.emit(this.user);
    this.user = {
       name: '',
       email: ''
    }
  }

  ngOnInit() {
  }

}