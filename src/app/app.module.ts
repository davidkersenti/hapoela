import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RouterModule, Routes } from '@angular/router';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicesService } from './invoices/invoices.service';
import{AngularFireModule} from 'angularfire2';
import { SpinnerComponent } from './shared/spinner/spinner.component';


export const firebaseConfig = {
   apiKey: "AIzaSyDW7wAWhH39nddp86v-kNv8JLE-6bFPIRY",
    authDomain: "invoicess-b8b84.firebaseapp.com",
    databaseURL: "https://invoicess-b8b84.firebaseio.com",
    projectId: "invoicess-b8b84",
    storageBucket: "invoicess-b8b84.appspot.com",
    messagingSenderId: "1001478619148"
}





const appRoutes: Routes = [
  { path: 'invoices', component: InvoicesComponent },
  { path: 'invoiceForm', component: InvoiceFormComponent },
  { path: '', component: InvoiceFormComponent },
 { path: '**', component: PageNotFoundComponent }
];






 
@NgModule({
  declarations: [
    AppComponent,
    InvoicesComponent,
    InvoiceFormComponent,
    PageNotFoundComponent,
    InvoiceComponent,
    SpinnerComponent,
    
    
    
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
    
    
  ],
  providers: [InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }